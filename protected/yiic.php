<?php

// change the following paths if necessary
$yiic=dirname(__FILE__).'/../../yii/yiic.php';
$config=dirname(__FILE__).'/config/console.php';

$config = require( $config );

$localConfigFileName = dirname( __FILE__ ).'/config/console-local.php';

if ( is_readable( $localConfigFileName ) ) {
    defined('YII_DEBUG') or define('YII_DEBUG', true);

    $local = require( $localConfigFileName );

    $config = CMap::mergeArray($config, $local);
}

require_once($yiic);
