<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/../yii/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';
$localConfigFileName = dirname(__FILE__) . '/protected/config/main-local.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);

$config = require( $config );

// including local config if exists

if ( is_readable( $localConfigFileName ) ) {
    $local = require( $localConfigFileName );
    $config = CMap::mergeArray($config, $local);
}

Yii::createWebApplication($config)->run();
